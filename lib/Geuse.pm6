use v6;
unit module Geuse:ver<0.0.1>:auth<gitlab:uzluisf>;

=begin pod
=head1 Geuse

Geuse is a (mostly simple) static site generator built using the Raku
programming language.

=head1 Synopsis

After installing Geuse, run the following command from an empty directory:

=code geuse init

This will create four directories (C«content», C«templates», C«include» and
C«output») and a YAML file named C«config.yml» which you can remove or modify
to your liking. These directories can be rendered into a website by running
C«geuse render», which will place HTML and some other files in the
directory C«output».

=head1 Usage

=begin code
Usage:
  geuse help
            show this help output
  geuse init
            initialize new project
  geuse render
            render all templates to build
  geuse render --clear
            delete build directory before rendering site
  geuse clear
            delete build directory and all of its contents
  geuse version
            print geuse version and exit
=end code

=head1 Installation

Using zef:
=for code
zef update && zef install Geuse

From source:
=for code
git clone https://gitlab.com/uzluisf/geuse.git
cd geuse && zef install .

=head2 Requirements and dependencies

First of all, the Raku programming language must be installed on the machine.
Second, the core of Geuse relies on the following modules which can all
be installed by using C«zef».

=item L«C«Template::Mustache»|https://github.com/softmoth/p6-Template-Mustache»
for parsing the Mustache templates.
=item L«C«Text::Slugify»|https://gitlab.com/uzluisf/raku-text-slugify» for
slugifying URLs.
=item L«C«YAMLish»|https://github.com/Leont/yamlish» for parsing the YAML configuration file.

As mentioned in the X«B«Plugin system»» section, most useful functionalities
(markdown rendering, previewing, etc.) can be made available to Geuse through
plugins. Those plugins might have dependencies of their own so make sure to read
their installation instructions.

=head1 Template features

By default, Geuse uses the Mustache templating system.

=head1 Plugin system

Geuse doesn't do much in and of itself. For instance, while you can write
your documents in Markdown, Geuse won't be able to render them by default.
Instead you must install (or write) a plugin-like module that accomplishes
the given task. A side effect of this is extensibility so you can write
(and use) modules that accomplish specific tasks.

=head2 Available plugins

=item L«C«Geuse::Markdown»|https://gitlab.com/uzluisf/geuse-markdown» for
rendering Markdown documents.
=item L«C«Geuse::Pod»|https://gitlab.com/uzluisf/geuse-pod» for rendering
L«Pod|https://docs.perl6.org/language/pod» documents.

=head2 Writing a Geuse plugin

Writing a Geuse plugin is quite simple if you understand how Geuse picks
up available plugins. In the CLI tool (C«geuse») for Geuse, a simple
context object from the aptly named C«GeuseContext» class, which carries several
states, is passed to each plugin's globally C«our»-declared
C«register($context, %plugin-config)» subroutine. The C«%plugin-config»
parameter stores the information provided for the module in the YAML
configuration file. Each plugin that Geuse should be aware of must be
specified in the configuration file under a C«plugins» section. For instance,
in

=begin code
plugins:
    - name: Geuse::Markdown
      extensions: ['md', 'mkd', 'markdown']
=end code

the module named C«Geuse::Markdown» is specified with some configuration
that goes into C«%plugin-config». This module will be picked up by
Geuse and C«$context» and C«%plugin-config» will be passed to its
C«register» subroutine.

=head2 Module naming convention

Ideally, new plugins follow the convention C«Geuse::X» with C«X» being
the functionality the module provides.

=head2 More info

For extra information and a walkthrough for creating a Geuse plugin,
go to L«https://uzluisf.gitlab.io/geusedoc/».

=head1 Authors

=item Luis F. Uceta [L«gitlab|https://gitlab.com/uzluisf», L«github|https://gitlab.com/uzluisf»]

=head1 Internet resources

=item Source code: L«https://gitlab.com/uzluisf/geuse»
=item Issues: L«https://gitlab.com/uzluisf/geuse/issues»
=item Website: L«https://uzluisf.gitlab.io/geusedoc/»

=head1 See also

=item L«Mustache|https://github.com/softmoth/p6-Template-Mustache/»

=head1 License

Geuse is free software; you can redistribute it and/or modify it under the terms
of the Artistic License 2.0. See the file LICENSE for details.

=end pod
