#| Class that handles the writing of files to output directory.
unit class Geuse::Writer is export;

has Str $!output-folder;
has     @!generators;

#| Constructor that takes a positional indicating the output folder.
method new( Str:D $output-folder ) {
    self.bless: :$output-folder;
}

submethod TWEAK( :$!output-folder ) {}

#| Add a generator.
multi method add( $generator ) {
    =begin comment
    A generator is just an object that handles certain output. For
    instance, a Builder object takes care of files created from the
    'content' directory. In the other hand, an Includer object takes
    care of files in the 'include' directory.
    =end comment

    @!generators.push: $generator
}

#| Add several generators.
multi method add( *@generators ) {
    self.add($_) for @generators
}

#| Write files to output directory.
method write( *@ ) {
    for self!files.flat -> $destination, $content {
        self!write-file($destination, $content)
    }
}

#| Return a list of destination-content pairs.
method !files {
    gather for @!generators -> $generator {
        for $generator[] {
            my ($destination, $content) = $_.key, $_.value;
            take $destination, $content
        }
    }
}

#| Write content to destination.
method !write-file( $destination, $content ) {
    my $d = $destination.subst(/^ .* <?before '/' include>/, '');
    my $full-destination   = $!output-folder.IO.add($d).Str;
    my $destination-folder = $full-destination.IO.dirname;

    # if destination directory doesn't exist, create it.
    mkdir $destination-folder unless $destination-folder.IO.d;

    $full-destination.IO.spurt: $content;
}
