unit module Geuse::Renderers;
use Geuse::Errors;
use Geuse::Utils;
use Template::Mustache;

#| Class that represents the templates.
class TemplateRenderer is export(:TemplateRenderer) {
    has $.template-folder; # directory where templates are located
    has $.include-folder;  # directory where assets are located
    has %!templates;

    submethod TWEAK() {
        =begin comment
        Load up %!templates with pairs of template's name and template's
        filepath.
        For instance, a template named 'welcome.mustache' will stored as:
        'welcome' => 'welcome.mustache'.
        =end comment
        for self!load-templates -> $name-template-pair {
            %!templates.push: $name-template-pair
        }
    }

    #| Render a page alongside site's information.
    method render-page( %page, $site --> Str:D ) {
        =begin comment
        Remember 'type' refers to the template to use to render a particular page.
        =end comment

        # make sure each page specifies a template (index, blog, etc.).
        if %page<type>:!exists {
            fail X::MissingTypeError.new(:%page);
        }

        # make sure each page specifies only one template.
        unless %page<type>.elems == 1 and %page<type> ~~ Str {
            fail X::NonSingleTypeError.new(:%page);
        }

        # make sure template specified in a page exists.
        if %!templates{ %page<type> }:!exists {
            fail X::MissingTemplateError.new(:%page);
        }

        # template seems to exists so get a hold of it.
        my $template = %!templates{ %page<type> };

        # get partials
        my %partials = self!get-partials;

        =begin comment
        For each page to render, the context supplied to Template::Mustache
        contains the fields:
            * 'page', which maps to the current page's information (author,
              date, content, etc.)
            * 'site', which maps to the site's information (name, base-url,
              etc.) including a field 'pages' that maps to an array of pages.
              This array also contains the current page.
            * 'include', which maps to whatever it's in the 'include'
              directory.

        All the information in these fields will be available to
        the template as
            * page.KEY,
            * site.KEY,
            * include.KEY,
        where KEY is some key would normally use to access the information
        in the fields.
        =end comment

        # collect posts (if existent) under a certain url and make them
        # available to pages with the same root at the context's top level.
        # The top level page also has access to collected posts.
        my $key = %page<url>.subst(/^ '/'? (\w+) .* $/, {"$0"});
        my $posts := $site{$key}:exists ?? $site{$key} !! [];

        my %context = %(
            :%page,
            :$posts,
            :$site,
            :include(Geuse::Utils::read-folder $!include-folder)
        );

        return self!render-template:
            :%context,
            :content($template.IO.slurp),
            :from[%partials]
        ;
    }

    # Get all partial templates. Geuse assume they're stored in
    # 'templates/partials/'
    method !get-partials( --> Hash ) {
        my $partials = $!template-folder.IO.absolute ~ "/partials";
        my %partials;
        for dir($partials) -> $partial {
            %partials{
                $partial.basename.subst(/\.mustache/,'')
            } = $partial.IO.slurp;
        }
        return %partials;
    }

    # Load templates from template folder. Return template's name
    # and template's filepath pairs.
    method !load-templates {
        gather for dir($!template-folder) -> $template-file {
            my $ext = $template-file.extension;
            my $template-name = $template-file.basename.Str.subst(/\.$ext/,'');
            take $template-name => $template-file;
        }
    }

    method !render-template( :%context, Str :$content, :@from --> Str:D ) {
        quietly {
            Template::Mustache.render: $content, %context, :@from;
        }
    }
}

#| Class that takes care of rendering the a document's content.
class ContentRenderer is export(:ContentRenderer) {
    =begin comment
    A hash mapping extensions (md, txt, pod6, etc.) to functions that perform
    the rendering of documents with that extension(s).
    =end comment

    has %.renderers;

    # override new constructor to prevent initialization
    # of %.renderers at object construction.
    method new {
        self.bless()
    }

    #| Render a page by using the render specified by its extension.
    method render( %page ) {
        # we must have the renderer indicated by the page's extension.
        # Throw error if we cannot find it.
        if %!renderers{ %page<extension> }:!exists {
            fail X::MissingRendererError.new(:%page);
        }

        my &renderer = %!renderers{%page<extension>};

        # NOTE: This is quite constrived but it'll work for the moment.
        my %bare-documents = %(
            'md'       => '',
            'mkd'      => '',
            'markdown' => '',
            'pod6'     => "=begin pod\n=end pod",
        );

        return &renderer(
            %page<raw-content> //             # if not raw content, ...
            %bare-documents{%page<extension>} # ...render this minimal doc
        );
    }

    #| Add a render to the group of renderers.
    method add-renderer( @extensions, &function --> Nil ) {
        =begin comment
        The goal here is to map a group of different extensions, which
        correspond to the same document type, to the same rendering function.
        For instance, Markdown (.md, .mkd, .markdown) is such an example.
        =end comment

        for @extensions -> $extension {
            next if $extension === Any;
            %!renderers{$extension} = &function
        }
    }

    #| Return a ContentRenderer instance for extensions '' and 'txt'.
    method default( ::?CLASS:U: --> ContentRenderer:D ) {
        my $instance = self.new;

        # let's render plaintext...
        my @plain-extensions = Nil, '', 'txt';
        my &render-plain = sub ($raw-content) { $raw-content };
        $instance.add-renderer(@plain-extensions, &render-plain);

        return $instance
    }
}
