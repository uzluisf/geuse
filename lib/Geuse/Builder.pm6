use Geuse::Config;
use Geuse::Renderers :TemplateRenderer, :ContentRenderer;
use Geuse::Utils;

constant Utils := Geuse::Utils;

unit class Geuse::Builder is export;
also does Iterable;

has %.directories;
has %.page-defaults;
has %.site;
has $.template-renderer;
has $.content-renderer;

method new( Config $config, ContentRenderer $content-renderer ) {
    self.bless: :$config, :$content-renderer
}

submethod TWEAK( :$config, :$!content-renderer ) {
    %!directories       = $config.directories;
    %!page-defaults     = $config.page-defaults;
    %!site              = $config.site;
    $!template-renderer = TemplateRenderer.new(
        template-folder => $config.directories<templates>,
        include-folder  => $config.directories<include>,
    );
}

#| Iterate over a Builder object and return destination-HTML page pairs.
method iterator() {
    my %page-generator = Utils::read-folder(%!directories<content>, :mode<ro>);
    %!site<pages>      = Utils::make-pages(%page-generator, %!page-defaults,
                         :content(%!directories<content>));

    %!site<groups> = (gather for Utils::group-pages(%!site).kv {
        take $^field => $^groups
    }).Hash;

    # this mutates $!site's each page's subpage key.
    Utils::give-subpages(%!site);

    # render raw content of pages.
    Utils::render-pages(%!site<pages>, $!content-renderer);

    # pick up directories of 'content'. This will be make available
    # through %!site to the templates.
    my @subdirs = Utils::read-folder(%!directories<content>, :top-level)
        .grep({.key ne 'content'})
        .Hash.keys;

    %!site.push: Utils::collect-posts(%!site, @subdirs);

    %!site<pages>.flat.map({
        my $page        = $_;
        my $destination = Geuse::Utils::build-destination($page, %!directories<output>);
        my $html-page   = $!template-renderer.render-page($page, %!site);
        $destination => $html-page;
    }).iterator;
}
