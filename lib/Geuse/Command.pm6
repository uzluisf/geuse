use Geuse::Utils :cli-arguments;
unit class Geuse::Command is export;

has %!commands{Capture};

submethod TWEAK {
    # add the help routine to the usage message.
    self.add:
        capture     => \('help'),
        routine     => -> |c { self.^find_method("help").(self, |c) },
        description => 'Show this help output'
    ;
}

#| Add a CLI command to geuse.
method add(
    :$capture!,    #= command's name
    :$routine!,    #= routine associated with command
    :$object      = Nil,      #= object associated with the routine
    :$description = "(No description available)"
) {
    %!commands{ $capture } = %(:$object, :$routine, :$description)
}

#| Run geuse with the given command and arguments.
method run( @positionals ) {
    my $ext-capt = Arguments::get-capture @positionals;
    my $int-capt = %!commands.keys.first(* eqv $ext-capt);

    # something needs to be executed after all...
    # if no internal capture, show the usage message instead.
    unless $int-capt {
        self.help('help');
        return
    }

    given %!commands{$int-capt} -> $cmd {
        given ($cmd<object>, $cmd<routine>) {
            when *, Code {
                $cmd<routine>(|$int-capt);
            }
            when *, Str {
                my ($obj, $meth) = $cmd<object>, $cmd<routine>;
                if $obj.^can($meth) {
                    $obj."$meth"(|$int-capt)
                }
                else {
                    put "Object {$obj.^name} cannot do $meth";
                    exit 1;
                }
            }
            default {
                put q:to/MSG/;
                » No such command.
                When adding a new command, please make sure to:
                * supply a method's name and the object it'll called on; or
                * provide a subroutine.
                MSG
                exit 1;
            }
        }
   }
}

method help('help') {
    my $help-capture = \('help');
    my $program-name = 'geuse';
    my $max-command  = %!commands.keys.map({Arguments::print-capture($_).chars}).max;

    put 'Usage:';
    for %!commands.keys.sort -> $capture {
        next if $help-capture eqv $capture;
        put "  $program-name ",
            Arguments::print-capture($capture),
            ' ' x ($max-command - Arguments::print-capture($capture).chars),
            ' - ',
            %!commands{$capture}<description>.lc;
    }

    put "  $program-name ",
        Arguments::print-capture($help-capture),
        ' ' x ($max-command - Arguments::print-capture($help-capture).chars),
        ' - ',
        %!commands{$help-capture}<description>.lc;
}
