unit module Geuse::Errors;

class X::MissingTypeError is export {
    also is Exception;
    has %.page;

    method message {
        "✗ Missing type for file «{%!page<path>}»"
    }
}

class X::NonSingleTypeError is export {
    also is Exception;
    has %.page;

    method message {
        "✗ Multiple types [{~%!page<type>}] for file «{%!page<path>}»"
    }
}


class X::MissingTemplateError is export {
    also is Exception;
    has %.page;

    method message {
        "✗ Missing template «{%!page<type>}» for file «{%!page<path>}»"
    }
}

class X::MissingRendererError is export {
    also is Exception;
    has %.page;

    method message {
        "✗ Cannot render page with extension »{%!page<path>}»"
    }
}

class X::NoUrlError is export { 
    also is Exception;
    has %.page;

    method message {
        "✗ No url or url-pattern in file «{%!page<path>}»"
    }
}
