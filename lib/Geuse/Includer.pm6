use Geuse::Utils;

#| Class that takes of the 'include' directory.
unit class Geuse::Includer is export;
also does Iterable;

has $.include;      # the 'include/' directory
has $.output;       # the 'output/' directory
has %.specific;     # stores extension-render function pairs

method new( %directories ) {
    self.bless: :%directories
}

submethod TWEAK( :%directories ) {
    $!include = %directories<include>;
    $!output  = %directories<output>;
}

#| Assign each extension a function that can render a document
#| of such extension.
method add( @extensions, &function ) {
    for @extensions -> $extension {
        %!specific{$extension} = &function
    }
}

#| Needs documentation...
method read( $path, $content is rw --> Pair ) {
    # get extension from path since it's needed to decide which
    # handler to use on the content on filepath.
    my $extension = $path.IO.extension;

    # Less Than Awesome?!
    my $sep = '/';
    my $suggested-path = $path.split($sep, :skip-empty).join($sep);

    # If such extension exists, use handler that corresponds to
    # given extension in order to handle (e.g., render) the file in path.
    if %!specific{$extension}:exists {
        my &handler = %!specific{$extension};
        ($suggested-path, $content) = &handler($suggested-path, $content);
    }

    # return a pair composed by suggested path and its content.
    return $suggested-path => $content;
}

method iterator( Geuse::Includer:D: ) {
    # read content of 'include' directory as binary.
    Geuse::Utils::read-folder($!include, :mode<ro>, :bin).flat
    .map({self.read($_.key, $_.value)}).Hash.iterator;
}
