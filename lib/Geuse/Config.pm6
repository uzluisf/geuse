use Geuse::Utils;
use YAMLish;

#| Class that represents the YAML configuration file.
unit class Geuse::Config is export;

has %.directories;
has %.page-defaults;
has %.site;
has @.plugins;

#| Return a Config object populated with data from the YAML config file.
method from-path( ::?CLASS:U: $path ) {
    =begin comment
    * The 'directories' field contains the directories Geuse will work with.
    By default, Geuse is aware of the following directories:
        * 'content/', where the website's contents lives.
        * 'output/', where the HTML for the website's content is saved.
        * 'templates/', where templates for generating the HTML live.
        * 'include/', where assets (css, js, images, etc.) live.

    * The 'page-defaults' field contains default configuration common to all
    the pages. Pages can override this configuration.

    * The 'site' field contains information about the website. For
    instance:
        - site:
            title: Website's Name

    * The 'plugins' field contains the plugins that must be loaded.
    For example:
        - plugins:
            # plugin for rendering markdown
            - name: Geuse::Markdown
              extensions: ['md', 'mkd', 'markdown']
            # plugin for rendering pod
            - name: Geuse::Pod
              extensions: ['pod6']
    =end comment

    my %default-dirs = %(
        content   => 'content',   # for site's content
        output    => 'output',    # for site's HTML output
        templates => 'templates', # for templates to fill content in
        include   => 'include',   # for static files (css, js, etc)
    );

    my %data = self!parse-config: $path;

    # fill out missing directories in %data<directories>
    for %default-dirs.kv -> $k, $v {
        %data<directories>{$k} = $v unless %data<directories>{$k}:exists
    }

    # Network
    %data<site><host> ||= '0.0.0.0';
    %data<site><port> = ($_<port>:exists ?? $_<port>.Int !! 3000) given %data<site>;

    # Paths
    %data<site><project-root> = "{%data<site><project-root>||$*CWD}".subst('~',$*HOME);
    for %data<directories>.keys {
        my $dir = %data<directories>{$_};
        %data<directories>{$_} = %data<site><project-root>.IO.add($dir).Str;
    }

    return self.new:
        directories   => %data<directories>   // Empty,
        site          => %data<site>          // Empty,
        page-defaults => %data<page-defaults> // Empty,
        plugins       => |%data<plugins>      // Empty,
    ;
}

method config-variables {
   %(
       page-defaults => 'Defaults for pages',
       plugins => 'Plugins Geuse should be aware of',
       site => 'Settings that are not core config variables',
       directories => 'Directories Geuse should be aware of',
   )
}

method !parse-config( $config-path --> Hash ) {
    unless $config-path.IO.f {
        note "Config file «{$config-path}» not found. Please run 'geuse init' to generate.";
        exit 1;
    }

    try {
        CATCH {
            default {
                note "Invalid YAML config file «{$config-path}».";
                exit 1;
            }
        }

        my %config = load-yaml($config-path.IO.slurp) when $config-path.IO.f;
        my @core-vars = 'directories', |self.config-variables.keys;

        # collect non-core variables into site => %(). Thus everything
        # other than site, page-defaults and plugins goes into 'site',
        # even if it's defined outside of 'site:' in the YAML config file.
        %config<site> = flat(%config.grep: {
            !@core-vars.contains: .key
        }).Hash;

        # remove collected non-core variables from hash's first level
        for %config.keys {
            %config{$_}:delete if !@core-vars.contains($_)
        }

        return %config;
    }
}
