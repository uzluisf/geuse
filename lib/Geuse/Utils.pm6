use Geuse::Errors;
use YAMLish;        # for perlifying the YAML config file
use Text::Slugify;  # for the 'slugify' subroutine

unit module Geuse::Utils;

=begin comment
NON-EXPORTED ROUTINES:
=end comment

# Walk a directory and return a hash mapping its directories to their files.
sub walk( $dir, Mu :$test ) {
    =begin comment
    Given a directory 'site' with the following structure:
    site/
    |
    |--contents
    |  |--file1.md
    |  |--file2.md
    |
    |--blog
    |  |--blog1.md
    |  |--blog2.md
    |
    |-extra

    Then the 'read-folder' sub returns the hash:
    %(
        'site/contents' => <file1.md file2.md>,
        'site/blogs' => <blog1.md blog2.md>
    )
    =end comment

    my %dir-to-files.push(
    gather for dir $dir -> $path {
        if $path.IO.f && $path.basename ~~ $test {
            take $path.dirname => $path.IO.basename;
        }
        if $path.d {
            .take for walk $path, :$test
        };
    });
    return %dir-to-files;
}

# Merge second hash into first hash.
sub merge-metadata( :%into, :%from ) {
    my %res = %into;
    for %from.keys -> $k {
        if %res{$k}:!exists or not %res{$k}.defined {
            %res{$k} = %from{$k}
        }
    }
    return %res;
}

class GroupKey {
    has $.name;
    has $.slug = slugify $!name;

    method WHICH {
        ValueObjAt.new: "{::?CLASS.perl}|{$!name.WHICH()}"
    }
}

multi infix:<eqv>(GroupKey $l, GroupKey $r) {
    $l.name eqv $r.name
}

our %month-names =
    en => <January February March April May June July
           August September October November December>;

our %day-names =
    en => <Monday Tuesday Wednesday Thursday Friday Saturday Sunday>;

# Returns the language-specific month name.
sub month-name($i, $lang) {
    %month-names{$lang.lc}[$i - 1];
}

# Returns the language-specific day name.
sub day-name($i, $lang) {
    %day-names{$lang.lc}[$i - 1];
}

=begin comment
EXPORTED ROUTINES:
=end comment

our proto read-folder( $folder, | ) {*}

#| Return a hash of a folder's subdirectories (their basename) to their files.
multi read-folder( $folder --> Hash ) {
    my %dir2content;
    for walk($folder).kv -> $dir, $files {
        for $files.list -> $file {
            %dir2content{ $dir.IO.basename }.push:
            $dir.IO.add($file).subst("{$*CWD}/", '')
        }
    }
    return %dir2content;
}

#| Return a hash of a folder's subdirectories to their files.
#| Only the top-level directories mapped to all their contents.
multi read-folder( $folder, :$top-level is required --> Hash ) {
    =begin comment
    Given
        content/
        ├── blog
        │   ├── 2018
        │   │   ├── blog1.md
        │   │   └── blog2.md
        │   └── 2019
        │       ├── blog1.md
        │       └── blog2.md
        ├── docs
        │   ├── docs1.md
        │   └── docs2.md
        └── file1.md
    then read-folder('content', :top-level) returns
        {blog => [blog/2018/blog1.md, blog/2018/blog2.md,
                  blog/2019/blog1.md, blog/2019/blog2.md],
         docs => [docs/docs1.md, docs/docs2.md]
        }
    =end comment

    my %dir2content;
    for walk($folder).kv -> $dir, $files {
        next if $folder eq $dir;
        my $subdir = $dir.subst(/^ $folder '/'? /, '');
        my $d = $subdir.IO.parent eq '.' ?? $subdir !! $subdir.IO.parent.Str;
        for $files.list -> $file {
            %dir2content{$d}.push: $dir.IO.add($file).subst("{$*CWD}/", '')
        }
    }
    return %dir2content;
}

#| Return a hash of a folder's paths mapped to their contents.
multi read-folder( $folder, :$mode is required, :$bin --> Hash ) {
    my $enc  = $bin ?? Nil !! 'utf-8';

    # Get all filepaths and map them to their contents.
    my %dir2rawcontent.push(
        gather for walk($folder).kv -> $dir, $files {
            for @$files -> $filename {
                my $path = $dir.IO.add($filename);
                my $capt = $bin ?? \($path, :$mode, :$bin)
                                !! \($path, :$mode, :$enc);
                given open(|$capt) -> $fo {
                    take $path => $fo.slurp
                }
            }
        }
    );

    return %dir2rawcontent;
}

#| Return destination of page based on page's url.
our sub build-destination( %page, $folder, :$omit-html-ext = False ) {
    my $url = %page<url>;
    my $ext = $omit-html-ext ?? '' !! '.html';

    # Add index.html to the destination for pretty url.
    $url = do given $url {
        when 'index'         { ('index',          $ext).join }
        when .ends-with('/') { ($_,      'index', $ext).join }
        default              { ($_, '/', 'index', $ext).join }
    }

    return $url
}

#| Create a hash representing a page given a path, raw content and page defaults.
our sub make-page( $path, $raw-content, %page-defaults, :$content --> Hash ) {
    my %page = extension => $path.IO.extension;

    # separate yaml from main content
    my @raw = $raw-content.split('---', 3).grep(*.chars > 0);

    # read the yaml and perlify it. Push to %page a combination
    # of the page's yaml and page's defaults (from config.yml). The page's
    # yaml wins over the page's defaults.
    my %yaml = load-yaml(@raw[0]) // %();
    %page.push: merge-metadata :into(%yaml), :from(%page-defaults.grep(*.key ne 'date').hash);

    # get page's raw content (yaml header removed).
    %page<raw-content> = @raw[1];

    # return immediately if page isn't to be published. 'draft' is either
    # true (don't publish page) or false (publish page).
    return if %page<draft>.so;

    # include the path.
    %page<path> = $path;

    # make slug.
    %page<slug> = make-slug(%page);

    # make url.
    %page<url> = make-url(%page, :$content);

    %page<date-obj> = Date.new(%page<date>) if %page<date>;

    # format page's date.
    if %page<date>:exists and %page-defaults<date>:exists {
        if %page<date-format>:exists {
            %page<date> = format-date %page<date>,
                :format(%page<date-format>),
                :lang(%page<lang> // 'en');
        }
        elsif %page-defaults<date><date-format>:exists {
            %page<date> = format-date %page<date>,
                :format(%page-defaults<date><date-format>),
                :lang(%page-defaults<date><lang> // 'en');
        }
    }

    return %page
}

#| Return an array of created pages.
our sub make-pages( %paths, %page-defaults, :$content = 'content' --> Array ) {
    if %page-defaults<date><months>.so {
        %month-names.append: %page-defaults<date><months>.flat
    }
    if %page-defaults<date><days>.so {
        %day-names.append: %page-defaults<date><days>.flat
    }
    (gather for %paths.kv -> $path, $raw-content {
        my $page = make-page($path, $raw-content, %page-defaults, :$content);
        if $page {
            take $page
        }
    }).Array
}

#| Return page's slug.
our sub make-slug( %page --> Str:D ) {
    =begin comment
    If a slug already exists for the page, return it. Else create one from
    title and return it. Otherwise (neither slug nor title exists),
    return Nil.
    =end comment

    return do given %page {
        when $_<slug>:exists  { $_<slug> }
        when $_<title>:exists { slugify($_<title>.lc) }
        default               { Nil }
    }
}

#| Return page's url/url pattern if exists.
our sub make-url( %page, :$content = 'content') {
    sub url-from-path {
        my $ext = %page<path>.IO.extension;
        %page<path>.subst(/^ .* $content '/' $<p>=(.*) '.' $ext/, {"$<p>"});
    }

    # return url from path if url-from-path is true.
    return url-from-path() if %page{'url-from-path'}.so;

    my $url = do
    if %page<url>.defined && %page<slug>.defined {
        %page<url>.ends-with('/')
        ?? %page<url> ~ %page<slug>
        !! %page<url> ~ '/' ~ %page<slug>
        ;
    }
    elsif %page<slug>.defined {
        # create url from path and slug
        my $dn  = %page<path>.IO.dirname.IO.basename;
        my $dir = $dn eq 'content' ?? '/' !! $dn.IO.basename;
        ($dir, '/', %page<slug>).join;
    }
    else  {
        url-from-path()
    }

    # remove leading / if it's still around.
    $url.subst(1, *) if $url.starts-with('/');

    return $url;
}

our sub give-subpages( %site ) {
    =begin comment
    subpages: category.blog
    =end comment

    for %site<pages>.list <-> $page {
        # skip if not subpages entry.
        next if $page<subpages>:!exists;
        my ($primary, $secondary) = $page<subpages>.split('.');
        if $secondary eq '*' {
            $page<subpages> = %site<groups>{$primary}
        }
        else {
            my $secondary-key = GroupKey.new: name => $secondary;
            $page<subpages> = %site<groups>{$primary}{$secondary-key};
        }
    }
}

our sub group-pages( %site ) {
    my %name-to-grouping = gather for %site<grouping> -> $options {
        my $field           = $options<field>;
        my $name            = $options<name> // $field;
        my Bool $desc-order = $options<order> eq 'desc';
        my $sort-key        = $options<key>;

        my %grouping{GroupKey};
        for %site<pages>.list -> $page {
            next if $page{$field}:!exists;
            my ($key, $value);

            if $page{$field} !~~ List {
                if $page{$field} ~~ GroupKey {
                    $key = $page{$field}
                }
                else {
                    $value = $page{$field};
                    $key = GroupKey.new: name => $value;
                }
                $page{$field} = $key;
                %grouping{$key}.append: $page;
            }
            else {
                my @values = $page{$field}.flat;
                $page{$field} = [];
                for @values -> $value {
                    my $key = GroupKey.new: name => $value;
                    $page{$field}.append: $key;
                    %grouping{$key}.append: $page;
                }
            }
        }

        for %grouping.kv -> $key, $pages {
            my &custom-sort = sub ($page) { $page{$sort-key} }
            # sort pages in either descending or ascending order by a
            # page's sorting key.
            %grouping{$key} = $desc-order
                ?? $pages.sort(&custom-sort).reverse # descending order
                !! $pages.sort(&custom-sort);        # ascending order

        }
        take $name => %grouping;
    }

    return %name-to-grouping;
}

#| Collect all the pages from directories under the 'content' directory
#| into a hash pairing each subdirectory to an array of its pages.
our sub collect-posts(
    %site,
    @subdirs,
    :&by = { $^b<date> cmp $^a<date> } # sort pages by. Most recent post by default.
    --> Hash
) {
    return (gather for @subdirs -> $subdir {
        take $subdir =>
             %site<pages>
             .grep({$_<url>.match: /^ '/'? $subdir "/" \w+ /})
             .sort(&by)
             .Array
    }).Hash
}

our sub render-pages( @pages, $renderer ) {
    for @pages -> $page {
        # add page's HTML as content
        $page<content> = $renderer.render($page)
    }
}

our sub run-plugin( $module, $context is rw, $plugin-config ) {
    =begin comment
    Each plugin configuration has the plugin's name and a list of extensions
    it renders. For instance, the following is a YAML entry for two plugins:

    - plugins:
        - name: Geuse::Markdown
          extensions: ['md','mkd','markdown']
        - name: Geuse::Pod
          extensions: ['pod6']

    An attempt to load the plugin (or module) is made. If successful,
    then the 'register' sub is imported into the current scope and called
    with the necessary arguments. Otherwise, this is skipped and continue
    with the following plugin.
    =end comment

    try {
        require ::($module);
        if ::($module) !~~ Failure and defined &::($module)::register {
            my &register = &::($module)::register;
            &register($context, $plugin-config);
        }
        else {
            note "✗ Unable to load $module. Does it define an our-declared 'register' sub?"
        }
        CATCH { default {} }
    }
}

# Module bundling several subs to handle command line arguments.
module Arguments is export(:cli-arguments) {
    multi get-args( @all, :$posit! --> List ) {
        my regex argument { ^ \w+ $ }
        @all.map(*.match(/<argument>/))
            .grep(*.so)
            .map({«$_<argument>»}) # create an allomorph
            .List
    }

    multi get-args( @all, :$assoc! --> Hash ) {
        my regex key-value { ^ '-' ** 1..2 $<key>=(\w+) ['=' $<value>=(\w+)]? $ }
        @all.map(*.match(/<key-value>/))
            .grep(*.so)
            .map({
                $_<key-value><key> => $_<key-value><value>
                ?? «$_<key-value><value>»
                !! True
            })
            .Hash
    }

    our sub get-capture( @all --> Capture ) {
        return Capture.new:
            list => get-args(@all, :posit),
            hash => get-args(@all, :assoc)
        ;
    }

    our sub print-capture( Capture:D $capture --> Str ) {
        join ' ',
        $capture.list.Str,
        $capture.hash.pairs.map({
            my $dash = $_.key.chars == 1 ?? '-' !! '--';
            $dash ~ ($_.value.isa(Bool) ?? "{$_.key}" !! "{$_.key}=<..>");
        }).join(' ')
    }
}

our sub init(
    :$config-file = 'config.yml',
    :$site-name   = 'Geuse Site',
    :$language    = 'en',
    --> Bool
) {
    my $template-engine = 'mustache';
    my $config-yaml = qq:to/EOD/;
    # the project root path. By default, the current directory (.)
    project-root: '.'

    # the base url from which the site is served. set to / if using a
    # local webserver
    base-url: {$*CWD.Str}/build/
    name: {$site-name}
    lang: {$language}
    description: A simple Geuse site

    # plugins Geuse will pick up
    plugins:
        - name: Geuse::Markdown
          extensions: [md, mkd, markdown]

    # name of directories. In this instance, the 'output' directory
    # is named 'build'.
    directories:
        output: build

    # variables for templates
    nav-links:
       - nav-name: About
         nav-url: about/
    EOD

    # create project directories
    my List $template-dirs = (
        "content".IO,
        "templates".IO,
        "templates/partials".IO,
        "include".IO,
        "include/css".IO,
    );

    $template-dirs.map( -> $dir { mkdir $dir });

    # main directories structure
    my %templates =
        content   => ['index.md', 'about.md'],
        templates => [
            'post',
            partials => <head header footer>
        ],
        include   => [
            css => ['main']
        ]
    ;

    %templates.kv.map: -> $root, $files {
        my @target-files;
        for @$files -> $file {
            if $file ~~ Pair {
                for $file.values.flat {
                    @target-files.push: $root eq  'templates'
                        ?? "{$file.key}/{$_}.{$template-engine}"
                        !! "{$file.key}/{$_}.{$file.key}"
                }
            }
            else {
                if $root eq 'templates' {
                    @target-files.push: "{$file}.{$template-engine}";
                }
                else {
                    @target-files.push: $file;
                }
            }
       }

        for @target-files -> $file {
            my $source-filename = "{$template-engine}/{$root}/{$file}";
            my $target-path = "{$root}/{$file}";
            $target-path.IO.spurt: %?RESOURCES{$source-filename}.IO.slurp;
        }
    }

    # write config file
    return $config-file.IO.spurt: $config-yaml;
}

our sub rm-dir( IO::Path $dir --> Bool ) {
    return unless $dir.IO.d;
    so shell "rm -rf $dir";
}

our sub clear( $out-dir ) {
    rm-dir $out-dir.IO
}

=begin comment
START: Functions to format dates
Functions are borrowed from
DateTime::Format (https://github.com/supernovus/perl6-datetime-format/)
=end comment

our sub strftime(
  DateTime $dt,
  Str $format is copy,
  Str :$lang = 'en',
  Bool :$subseconds,
) {
    my %substitutions =
        # Standard substitutions for yyyy mm dd hh mm ss output.
        'Y' => { $dt.year.fmt(  '%04d') },
        'm' => { $dt.month.fmt( '%02d') },
        'd' => { $dt.day.fmt(   '%02d') },
        'H' => { $dt.hour.fmt(  '%02d') },
        'M' => { $dt.minute.fmt('%02d') },
        'S' => { $dt.whole-second.fmt('%02d') },
        # Special substitutions (Posix-only subset of DateTime or libc)
        'a' => { day-name($dt.day-of-week, $lang).substr(0,3) },
        'A' => { day-name($dt.day-of-week, $lang) },
        'b' => { month-name($dt.month, $lang).substr(0,3) },
        'B' => { month-name($dt.month, $lang) },
        'C' => { ($dt.year/100).fmt('%02d') },
        'e' => { $dt.day.fmt('%2d') },
        'F' => { $dt.year.fmt('%04d') ~ '-' ~ $dt.month.fmt(
                  '%02d') ~ '-' ~ $dt.day.fmt('%02d') },
        'I' => { (($dt.hour+23)%12+1).fmt('%02d') },
        'j' => { $dt.day-of-year.fmt('%03d') },
        'k' => { $dt.hour.fmt('%2d') },
        'l' => { (($dt.hour+23)%12+1).fmt('%2d') },
        'n' => { "\n" },
        'N' => { (($dt.second % 1)*1000000000).fmt('%09d') },
        'p' => { ($dt.hour < 12) ?? 'AM' !! 'PM' },
        'P' => { ($dt.hour < 12) ?? 'am' !! 'pm' },
        'r' => { (($dt.hour+23)%12+1).fmt('%02d') ~ ':' ~
                  $dt.minute.fmt('%02d') ~ ':' ~ $dt.whole-second.fmt('%02d')
                  ~ (($dt.hour < 12) ?? 'am' !! 'pm') },
        'R' => { $dt.hour.fmt('%02d') ~ ':' ~ $dt.minute.fmt('%02d') },
        's' => { $dt.posix.fmt('%d') },
        't' => { "\t" },
        'T' => { $dt.hour.fmt('%02d') ~ ':' ~ $dt.minute.fmt('%02d') ~ ':' ~ $dt.whole-second.fmt('%02d') },
        'u' => { ~ $dt.day-of-week.fmt('%d') },
        'w' => { ~ (($dt.day-of-week+6) % 7).fmt('%d') },
        'x' => { $dt.year.fmt('%04d') ~ '-' ~ $dt.month.fmt('%02d') ~ '-' ~ $dt.day.fmt('%2d') },
        'X' => { $dt.hour.fmt('%02d') ~ ':' ~ $dt.minute.fmt('%02d') ~ ':' ~ $dt.whole-second.fmt('%02d') },
        'y' => { ($dt.year % 100).fmt('%02d') },
        '%' => { '%' },
        '3N' => { (($dt.second % 1)*1000).fmt('%03d') },
        '6N' => { (($dt.second % 1)*1000000).fmt('%06d') },
        '9N' => { (($dt.second % 1)*1000000000).fmt('%09d') },
        'z' => {
            my $o = $dt.offset;
            $o
            ?? sprintf '%s%02d%02d',
               $o < 0 ?? '-' !! '+',
               ($o.abs / 60 / 60).floor,
               ($o.abs / 60 % 60).floor
            !! 'Z'
        },
        'Z' => {
            my $o = $dt.offset;
            $o
            ?? sprintf '%s%02d%02d',
               $o < 0 ?? '-' !! '+',
               ($o.abs / 60 / 60).floor,
               ($o.abs / 60 % 60).floor
            !! '+0000'
        },
    ; ## End of %substitutions

    $format .= subst( /'%'(\dN|\w|'%')/, -> $/ { (%substitutions{~$0}
            // die "Unknown format letter '$0'").() }, :global );
    return ~$format;
}

our sub format-date(
	Str $date,
	Str :$format = '%B %d, %Y',
	Str :$lang = 'en',
	Bool :$subseconds
) {
    return do if $date.match(/$<y>=\d**4 '-' $<m>=\d**2 '-' $<d>=\d**2/) {
        strftime DateTime.new(:year($<y>.Int), :month($<m>.Int), :day($<d>.Int)),
        $format, :$lang, :$subseconds;
    } else { $date }
}

=begin comment
END: Functions to format dates
=end comment

