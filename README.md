Geuse
=====

Geuse is a (mostly simple) static site generator built using the Raku programming language.

Synopsis
========

After installing Geuse, run the following command from an empty directory:

    geuse init

This will create four directories (`content`, `templates`, `include` and `output`) and a YAML file named `config.yml` which you can remove or modify to your liking. These directories can be rendered into a website by running `geuse render`, which will place HTML and some other files in the directory `output`.

Usage
=====

    Usage:
      geuse help
                show this help output
      geuse init
                initialize new project
      geuse render
                render all templates to build
      geuse render --clear
                delete build directory before rendering site
      geuse clear
                delete build directory and all of its contents
      geuse version
                print geuse version and exit

Installation
============

Using zef:

    zef update && zef install Geuse

From source:

    git clone https://gitlab.com/uzluisf/geuse.git
    cd geuse && zef install .

Requirements and dependencies
-----------------------------

First of all, the Raku programming language must be installed on the machine. Second, the core of Geuse relies on the following modules which can all be installed by using `zef`.

  * [`Template::Mustache`](https://github.com/softmoth/p6-Template-Mustache) for parsing the Mustache templates.

  * [`Text::Slugify`](https://gitlab.com/uzluisf/raku-text-slugify) for slugifying URLs.

  * [`YAMLish`](https://github.com/Leont/yamlish) for parsing the YAML configuration file.

As mentioned in the **Plugin system** section, most useful functionalities (markdown rendering, previewing, etc.) can be made available to Geuse through plugins. Those plugins might have dependencies of their own so make sure to read their installation instructions.

Template features
=================

By default, Geuse uses the Mustache templating system.

Plugin system
=============

Geuse doesn't do much in and of itself. For instance, while you can write your documents in Markdown, Geuse won't be able to render them by default. Instead you must install (or write) a plugin-like module that accomplishes the given task. A side effect of this is extensibility so you can write (and use) modules that accomplish specific tasks.

Available plugins
-----------------

  * [`Geuse::Markdown`](https://gitlab.com/uzluisf/geuse-markdown) for rendering Markdown documents.

  * [`Geuse::Pod`](https://gitlab.com/uzluisf/geuse-pod) for rendering [Pod](https://docs.perl6.org/language/pod) documents.

Writing a Geuse plugin
----------------------

Writing a Geuse plugin is quite simple if you understand how Geuse picks up available plugins. In the CLI tool (`geuse`) for Geuse, a simple context object from the aptly named `GeuseContext` class, which carries several states, is passed to each plugin's globally `our`-declared `register($context, %plugin-config)` subroutine. The `%plugin-config` parameter stores the information provided for the module in the YAML configuration file. Each plugin that Geuse should be aware of must be specified in the configuration file under a `plugins` section. For instance, in

    plugins:
        - name: Geuse::Markdown
          extensions: ['md', 'mkd', 'markdown']

the module named `Geuse::Markdown` is specified with some configuration that goes into `%plugin-config`. This module will be picked up by Geuse and `$context` and `%plugin-config` will be passed to its `register` subroutine.

Module naming convention
------------------------

Ideally, new plugins follow the convention `Geuse::X` with `X` being the functionality the module provides.

More info
---------

For extra information and a walkthrough for creating a Geuse plugin, go to [https://uzluisf.gitlab.io/geusedoc/](https://uzluisf.gitlab.io/geusedoc/).

Authors
=======

  * Luis F. Uceta [[gitlab](https://gitlab.com/uzluisf), [github](https://gitlab.com/uzluisf)]

Internet resources
==================

  * Source code: [https://gitlab.com/uzluisf/geuse](https://gitlab.com/uzluisf/geuse)

  * Issues: [https://gitlab.com/uzluisf/geuse/issues](https://gitlab.com/uzluisf/geuse/issues)

  * Website: [https://uzluisf.gitlab.io/geusedoc/](https://uzluisf.gitlab.io/geusedoc/)

See also
========

  * [Mustache](https://github.com/softmoth/p6-Template-Mustache/)

License
=======

Geuse is free software; you can redistribute it and/or modify it under the terms of the Artistic License 2.0. See the file LICENSE for details.

