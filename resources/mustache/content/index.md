---
url: /
type: post
date: 2019-06-12
draft: false
---

# Introduction

Hello, there! This is a demo site created using the static site generator 
[Geuse](https://gitlab.com/uzluisf/geuse), which is written in the
[Raku](https://perl6.org/) programming language and inspired by
[beetle](https://github.com/cknv/beetle).

# Getting started

To render this website, run `geuse render` and an output directory will be
created. This directory should contain the rendered HTML files with other
assets.

For further information, visit the documentation for Geuse which can be found
at [https://uzluisf.gitlab.io/geusedoc/](https://uzluisf.gitlab.io/geusedoc/).

Happy hacking!
