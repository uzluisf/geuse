---
type: post
url: about
date: 2019-06-12
subpages: category.about
tags: ['Geuse', 'Name']
draft: false
---

Geuse (as in Betel*geuse*) is a static site generator (SSG) for *geusing*
things up. It converts a bunch of templates, content, and resources (e.g., 
CSS, images, etc.) into another bunch of plain HTML. In order to do this, it uses
[Mustache](https://mustache.github.io/mustache.5.html) as its templating
system.

**What about the name?** This project borrows a lot from beetle. When I came
across beetle, the first word that came to mind was
[Betelgeuse](https://en.wikipedia.org/wiki/Betelgeuse). I guess watching that
many *The Universe* episodes paid off. Since the words *beetle* and *betel*
are homophones, I went for the second part of the name.
